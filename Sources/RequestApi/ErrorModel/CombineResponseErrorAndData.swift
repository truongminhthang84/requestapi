//
//  responseError.swift
//  SapoAdminIOS
//
//  Created by ThangTM-PC on 11/7/19.
//  Copyright © 2019 ThangTM-PC. All rights reserved.
//

import Foundation

public struct CombineResponseErrorAndData: Error {
    public var dataError: Data?
    public var responseError: HttpResponseError?
}
