//
//  HttpRequestError.swift
//  SapoAdminIOS
//
//  Created by ThangTM-PC on 11/7/19.
//  Copyright © 2019 ThangTM-PC. All rights reserved.
//

import Foundation

public enum HttpRequestError: Int, Error {
    case urlNil
    case httpBodyNil
    case indexOutOfBounce
}
