//
//  CodableError.swift
//  SapoAdminIOS
//
//  Created by ThangTM-PC on 11/7/19.
//  Copyright © 2019 ThangTM-PC. All rights reserved.
//

import Foundation

public enum CodableError: Error {
    case encodeFail
    case decodeFail    
}

public enum SerializerError: Error {
    case encodeFail
    case decodeFail
}
