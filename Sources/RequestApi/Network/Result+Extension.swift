//
//  Result+Extension.swift
//  SapoAdminIOS
//
//  Created by ThangTM-PC on 11/8/19.
//  Copyright © 2019 ThangTM-PC. All rights reserved.
//

import Foundation

extension Result where Success == Data {
    public func decoded<T: Decodable>(
        using decoder: JSONDecoder = JSONDecoder.shared) throws -> T {
            let data = try get()
            return try decoder.decode(T.self, from: data)
       
    }
}

// MARK: - Result

extension Result {
    private func getError() -> Error? {
        switch self {
        case .success:
            return nil
            
        case .failure(let error):
            return  error
        }
    }
   public func getErrorData<T: Decodable>(using decoder: JSONDecoder =  JSONDecoder.shared) throws -> T? {
        if let responseError = getError() as? CombineResponseErrorAndData, let dataError = responseError.dataError {
            return  try decoder.decode(T.self, from: dataError)
        }
        return nil
    }
   public func getErrorJson() throws -> JSON? {
        if let responseError = getError() as? CombineResponseErrorAndData, let dataError = responseError.dataError {
            return  (try JSONSerialization.jsonObject(with: dataError, options: [])) as? JSON
        }
        return nil
    }
    
   public func getErrorCode() throws -> Error? {
        if let responseError = getError() as? CombineResponseErrorAndData {
            return  responseError.responseError
        }
        return nil
        
    }
}

// MARK: - <#Mark#>

extension Error {
      public func getErrorData<T: Decodable>(using decoder: JSONDecoder =  JSONDecoder.shared) throws -> T? {
          if let responseError = self as? CombineResponseErrorAndData, let dataError = responseError.dataError {
              return  try decoder.decode(T.self, from: dataError)
          }
          return nil
      }
    
    public func getErrorJson() throws -> JSON? {
         if let responseError = self as? CombineResponseErrorAndData, let dataError = responseError.dataError {
             return  (try JSONSerialization.jsonObject(with: dataError, options: [])) as? JSON
         }
         return nil
     }
     
    public func getErrorCode() throws -> Error? {
         if let responseError =  self as? CombineResponseErrorAndData {
             return  responseError.responseError
         }
        return nil 
     }
    
}
