//
//  HTTPClient.swift
//  SapoAdminIOS
//
//  Created by ThangTM-PC on 11/7/19.
//  Copyright © 2019 ThangTM-PC. All rights reserved.
//

import Foundation
public class HTTPClient {
    
    private let session: URLSession
    
    public init(session: URLSession = URLSession.shared) {
        self.session = session
    }
    
    public func uploadData<T: Encodable, R: Decodable> (request: URLRequest,
                                                 sendingData: T,
                                                 completion: @escaping (Result<R, Error>) -> Void,
                                                 isDebug: Bool = false
    ) -> URLSessionUploadTask? {
        #if DEBUG
        print(isDebug: isDebug, "\n\n\n\nUploading data to URL: \(request.url?.absoluteString ?? "")")
        #endif
        
        guard let encodedData = try? JSONEncoder.shared.encode(sendingData) else {
            DispatchQueue.main.async {
                completion(.failure(HttpResponseError.encodeSendingDataFail))
            }
            return nil
        }
        #if DEBUG
        print(isDebug: isDebug, "Upload data: \n" + String(data: encodedData, encoding: .utf8)!)
        #endif
        
        let uploadTask = session.uploadTask(with: request, from: encodedData) { (data, response, error) in
            DispatchQueue.main.async {
                guard error == nil else {
                    completion(.failure(error!))
                    return
                }
                guard let statusCode = (response as? HTTPURLResponse)?.status else {
                    completion(.failure(HttpResponseError.noStatusCode))
                    return
                }
                guard let receivedData = data else {
                    completion(.failure(HttpResponseError.noReturnData))
                    return
                }
                switch statusCode.responseType {
                case .informational: break
                case .success,
                     .redirection:
                    if let parsedObject = try? JSONDecoder.shared.decode(R.self, from: receivedData) {
                        completion(.success(parsedObject))
                    } else if let empty = EmptyResponse() as? R {
                        completion(.success(empty))
                    } else {
                        completion(.failure(HttpResponseError.decodeReceivedDataFail))
                    }
                    
                default:
                    let error = response?.error ?? HttpResponseError.undefine
                    let combineResponseErrorAndData = CombineResponseErrorAndData(dataError: receivedData, responseError: error)
                    completion(.failure(combineResponseErrorAndData))
                }
            }
        }
        uploadTask.resume()
        return uploadTask
        
    }
    
    public func getData<R: Decodable> (request: URLRequest,
                                completion: @escaping (Result<R, Error>) -> Void,
                                isDebug: Bool = false ) -> URLSessionDataTask? {
        #if DEBUG
        print(isDebug: isDebug, "\n\n\n\nUploading data to URL: \(request.url?.absoluteString ?? "")")
        #endif
        
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                #if DEBUG
                print(isDebug: isDebug, "\n\n\n\nResponse: \(String(describing: response))")
                #endif
                
                guard error == nil else {
                    completion(.failure(error!))
                    #if DEBUG
                    print(isDebug: isDebug, "\n\n\n\nError: \(error!)")
                    #endif
                    return
                }
                guard let statusCode = (response as? HTTPURLResponse)?.status else {
                    completion(.failure(HttpResponseError.noStatusCode))
                    return
                }
                guard let receivedData = data else {
                    completion(.failure(HttpResponseError.noReturnData))
                    return
                }
                switch statusCode.responseType {
                case .informational: break
                case .success,
                     .redirection:
                    if let parsedObject = try? JSONDecoder.shared.decode(R.self, from: receivedData) {
                        completion(.success(parsedObject))
                    } else if let empty = EmptyResponse() as? R {
                        completion(.success(empty))
                    } else {
                        completion(.failure(HttpResponseError.decodeReceivedDataFail))
                    }
                    
                default:
                    let error = response?.error ?? HttpResponseError.undefine
                    let combineResponseErrorAndData = CombineResponseErrorAndData(dataError: receivedData, responseError: error)
                    completion(.failure(combineResponseErrorAndData))
                }
            }
        }
        dataTask.resume()
        return dataTask
        
    }
}
