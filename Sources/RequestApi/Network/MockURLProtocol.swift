//
//  MockURLProtocol.swift
//  SapoAdminUnitTests
//
//  Created by Le Toan on 10/24/19.
//  Copyright © 2019 Minh Thang. All rights reserved.
//

import Foundation

public class MockURLProtocol: URLProtocol {
    
    public static var requestHandler: ((URLRequest) throws -> (HTTPURLResponse, Data))?
    
    override public class func canInit(with request: URLRequest) -> Bool {
        return true
    }
    override public class func canInit(with task: URLSessionTask) -> Bool {
        return true
    }
    
    override open class func canonicalRequest(for request: URLRequest) -> URLRequest {
        var copy = request
        fixiOSBugOfEmptyHTTPBody(&copy)
        return copy
    }
    
    private class func fixiOSBugOfEmptyHTTPBody(_ request: inout URLRequest) {
        request.httpBody = request.httpBodyStream?.readfully()
    }
    
    override public func startLoading() {
        
        guard let handler = MockURLProtocol.requestHandler else {
            return
        }
        do {
            let (response, data) = try handler(request)
            client?.urlProtocol(self, didReceive: response, cacheStoragePolicy: .notAllowed)
            
            client?.urlProtocol(self, didLoad: data)
            client?.urlProtocolDidFinishLoading(self)
        } catch {
            client?.urlProtocol(self, didFailWithError: error)
        }
    
    }
    
    override public func stopLoading() {
        
    }
    
}

extension InputStream {
    func readfully() -> Data {
        var result = Data()
        var buffer = [UInt8](repeating: 0, count: 4096)

        open()

        var amount = 0
        repeat {
            amount = read(&buffer, maxLength: buffer.count)
            if amount > 0 {
                result.append(buffer, count: amount)
            }
        } while amount > 0

        close()

        return result
    }
}
