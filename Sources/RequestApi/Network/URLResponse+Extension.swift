//
//  URLResponse+Extension.swift
//  SapoAdminIOS
//
//  Created by ThangTM-PC on 11/7/19.
//  Copyright © 2019 ThangTM-PC. All rights reserved.
//

import Foundation

extension HTTPURLResponse {
    
    public var status: HTTPStatusCode? {
        return HTTPStatusCode(rawValue: statusCode)
    }
    public var responseError: HttpResponseError? {
        return HttpResponseError(rawValue: statusCode)
    }
    
}

extension URLResponse {
    public var statusCode: HTTPStatusCode? {
        return (self as? HTTPURLResponse)?.status
    }
    
    public var error: HttpResponseError? {
        return (self as? HTTPURLResponse)?.responseError
    }
    
}
