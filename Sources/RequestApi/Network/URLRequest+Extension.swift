//
//  File.swift
//  
//
//  Created by ThangTM-PC on 11/26/19.
//

import Foundation

extension URLRequest {
      
    public var method: HTTPMethod? {
        get {
            guard let httpMethod = self.httpMethod else { return nil }
            let method = HTTPMethod(rawValue: httpMethod)
            return method
        }
        
        set {
            self.httpMethod = newValue?.rawValue
        }
    }
}
