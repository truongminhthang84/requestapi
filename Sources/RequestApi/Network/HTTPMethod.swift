//
//  File.swift
//  
//
//  Created by ThangTM-PC on 11/26/19.
//

import Foundation

public enum HTTPMethod: String {
    
    case get        = "GET"
    case put        = "PUT"
    case post       = "POST"
    case delete     = "DELETE"
    case head       = "HEAD"
    case options    = "OPTIONS"
    case trace      = "TRACE"
    case connect    = "CONNECT"
}
