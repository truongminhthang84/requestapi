import XCTest

import ApiTests

var tests = [XCTestCaseEntry]()
tests += ApiTests.allTests()
XCTMain(tests)
